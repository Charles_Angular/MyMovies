import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { MovieModel } from '../../models/movie-model';

@Component({
  selector: 'app-movie-card',
  templateUrl: './movie-card.component.html',
  styleUrls: ['./movie-card.component.scss']
})
export class MovieCardComponent implements OnInit {
  @Output() movieClicked = new EventEmitter();
  @Input() movie: MovieModel;

  constructor() { }

  ngOnInit() {
  }

  itemClicked(movie) {
    this.movieClicked.emit(movie);
  }

}
