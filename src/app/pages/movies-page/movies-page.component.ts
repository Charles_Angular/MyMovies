import { Component, OnInit, Input, Output } from '@angular/core';
import { MovieModel } from '../../models/movie-model';
import { MoviesService } from '../../services/movies.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-movies-page',
  templateUrl: './movies-page.component.html',
  styleUrls: ['./movies-page.component.scss']
})
export class MoviesPageComponent implements OnInit {
  @Input() perpage: number;

  movies: Array<MovieModel>;
  loading = true;
  id: any;

  ngOnInit(): void {

  }

  constructor(
    private moviesService: MoviesService,
    private router: Router
  ) {
    moviesService.getMovies().then(data => {
      const response = data as any;
      this.loading = false;

      this.movies = response.results.map(item => {
        const moviemodel = new MovieModel();
        moviemodel.original_title = item.original_title;
        moviemodel.overview = item.overview;
        moviemodel.poster_path = item.poster_path;
        moviemodel.id = item.id;
        return moviemodel;
      });
    });
  }

  itemClicked(movie) {
    this.router.navigate(['/details', movie.id]);
  }

}
