import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { resolve } from 'url';

@Injectable({
  providedIn: 'root'
})
export class MoviesService {
  api_key = 'api_key=f6b9adefc9f1e328fc5a66a32f4a3354';
  base_path = 'https://api.themoviedb.org/3/';
  language = '&language=pt-BR';

  movies_popular_path = `${this.base_path}movie/popular?${this.api_key}${this.language}`;

  constructor(
    private http: HttpClient
  ) { }

  getMovies() {
    return new Promise((resolve, reject) => {
      this.http.get(this.movies_popular_path).subscribe(data => {
        console.log(data);
        resolve(data);
      }, err => {
        console.log(err);
        reject(err);
      });
    });
  }

  getMovie(id) {
    return new Promise((resolve, reject) => {
      const url = `${this.base_path}movie/${id}?${this.api_key}${this.language}`;
      this.http.get(url).subscribe(data => {
        console.log(data);
        resolve(data);
      }, err => {
        console.log(err);
        reject(err);
      });
    });
  }
}
