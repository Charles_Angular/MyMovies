import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { MoviesService } from '../../services/movies.service';
import { MovieModel } from '../../models/movie-model';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {
  id: string;
  movie: MovieModel = new MovieModel();
  constructor(
    private moviesService: MoviesService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.route.params.subscribe(params => {
      this.id = params['id'];
      this.moviesService.getMovie(this.id).then(data => {
        const response = data as any;
        this.movie.id = response.id;
        this.movie.original_title = response.original_title;
        this.movie.overview = response.overview;
        this.movie.poster_path = response.poster_path;
      });
    });
  }

  ngOnInit() {
  }

}
